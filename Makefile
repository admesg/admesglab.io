DOMAIN=andersdamsgaard.com

.PHONY: local
local:
	hugo server -D

.PHONY: generate-public
generate-public:
	# generate public/ and remove unprocessed images
	hugo --verbose
	find public \
		-iname '*.jpg' \
		-not -iname '*resize*.jpg' \
		-not -iname '*fill*.jpg' \
		-not -iname '*fit*.jpg' \
		-delete
	# report size of build
	du -h public/

.PHONY: generate-letsencrypt-certificate
generate-letsencrypt-certificate:
	sudo certbot certonly -a manual \
		-d $(DOMAIN) -d www.$(DOMAIN) \
		-d $(DOMAIN:.com=.dk) -d www.$(DOMAIN:.com=.dk)
	sudo cp /etc/letsencrypt/live/andersdamsgaard.com/{fullchain,privkey}.pem \
		~/.password-store/website/andersdamsgaard.com
	sudo chown -R ad ~/.password-store/website/andersdamsgaard.com
	@echo "Certificate is fullchain.pem, key (PEM) is privkey.pem"
	@echo "Upload to the gitlab pages configuration"

#.PHONY: renew-letsencrypt-certificate
#renew-letsencrypt-certificate: generate-letsencrypt-certificate
.PHONY: renew-letsencrypt-certificate
renew-letsencrypt-certificate:
	@echo "The following renew command may not work. If that is the case," \
		"manually renew with 'make generate-letsencrypt-certificate'"
	sudo certbot renew

.PHONY: check-letsencrypt-certificate
check-letsencrypt-certificate:
	echo | openssl s_client -showcerts -servername $(DOMAIN).com \
		-connect $(DOMAIN).com:443 2>/dev/null | \
		openssl x509 -inform pem -noout -text

