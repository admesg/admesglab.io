---
title: Om Anders
---

{{< imgproc "anders_damsgaard_portrait.jpg" Fit "300x300" >}}

&nbsp;

Jeg er en dansk fotograf med interesse for sort-hvide billeder. Jeg foretrækker 
lange eksponeringer og filmkameraer, hvor omhyggelig omtanke for visuel 
komposition og timing er essentiel. Lange eksponeringer får vand i bevægelse 
til at fremstå som glas, skyer bliver til slørede og abstrakte linjer, og 
travle mennesker forsvinder fra billedet.

I min professionelle tilværelse forsker jeg i klima, granulære materialer, og 
gletschere. Besøg min [akademiske hjemmeside](https://adamsgaard.dk) for mere 
information herom.
