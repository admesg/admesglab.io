---
title: About Anders
---

{{< imgproc "anders_damsgaard_portrait.jpg" Fit "300x300" >}}

&nbsp;

I am a danish photographer with an interest in black and white imagery. I tend 
to favor long exposures and film cameras, where careful consideration of visual 
composition and timing are essential. Long exposures make water appear as matte 
glass, skies turn to blurred entities and diffuse lines, and moving people 
vanish from view.

In my other life I am a scientist interested in climate, granular materials, 
and glaciers. For more information, please see my [academic 
webpage](https://adamsgaard.dk).
