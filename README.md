# andersdamsgaard.com
[![pipeline status](https://gitlab.com/admesg/admesg.gitlab.io/badges/master/pipeline.svg)](https://gitlab.com/admesg/admesg.gitlab.io/commits/master)

This repository contains the source for 
[andersdamsgaard.com](https://andersdamsgaard.com) and 
[andersdamsgaard.dk](https://andersdamsgaard.dk). The .com domain is registered 
through [domains.google.com](https://domains.google.com), and the .dk domain is 
registered through [dandomain.dk](https://dandomain.dk). When a client connects 
to the `.dk` domain, a [Javascript 
redirect](https://gitlab.com/admesg/admesg.gitlab.io/blob/master/themes/tale-mod/layouts/partials/head.html#L18)
makes sure to display the danish localization under 
`https://andersdamsgaard.com/da/`. Gitlab pages serve the content with the root 
path [https://admesg.gitlab.io](https://admesg.gitlab.io) under the repository 
with the name 
[https://gitlab.com/admesg.gitlab.io](https://gitlab.com/admesg.gitlab.io).

## DNS setup
The FastMail nameservers are defined as primary nameservers for both domains, 
and consist of the following records:

For andersdamsgaard.com:

    Type     Domain                    Data                 Comment
    ----------------------------------------------------------------------------
    A        andersdamsgaard.com       35.185.44.232        Points to gitlab.com
    CNAME    www.andersdamsgaard.com   admesg.gitlab.io     Point `www.` to root

For andersdamsgaard.dk:

    Type     Domain                    Data                 Comment
    ----------------------------------------------------------------------------
    A        andersdamsgaard.dk        35.185.44.232        Points to gitlab.com
    CNAME    www.andersdamsgaard.dk    admesg.gitlab.io     Point `www.` to root

In addition, `TXT` records are used for Gitlab-pages verification. Mail records 
are for both domains set to FastMail servers.

## Deployment
A preview of the webpage can be generated on the localhost with `make`. 
Deployment occurs through the [GitLab CI 
pipeline](https://gitlab.com/admesg/admesg.gitlab.io/pipelines) after pushing 
to the master branch.
